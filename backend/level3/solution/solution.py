#!/usr/bin/env python3

"""
Script for JSON data processing
Author: Medeiros Neto
Environment: Python 3.5 on Linux Mint 18.3
"""

import json
"""
Module for JSON loading, saving and printing.
"""

def process_json(input_json = "data.json", output_json = "output.json", save = False):
  """
  Function to process input JSON file and generate output JSON file.
  The input data contains articles information (with `id`, `name` and `price` fields), carts (with `id` and `items`
  fields), delivery fees ( with `eligible_transaction_volume` and `price` fields) and discounts (with `article_id`,
  `type` and `amount` fields).  The function iterates the items in each cart and gets the `unit_price` and `quantity`.
  If the item is eligible for a discount, with matching `article_id`, the discount is applied and returns the trucanted
  `net_price`.Then, the `net_price` is multiplied by item `quantity` and is summed in `total`. The delivery fee `price`
  is applied if the `total` is between the `min_price` and `max_price` of an `eligible_transaction_volume` iteration.
  Then, each cart is returned with the original `id` and `total` value. If the `save` argument is `True`, the 
  generated data is converted to a dictonary and saved as a JSON file formatted to match the `output.json` file style
  and returns the `output_file` filename. Otherwise, the JSON data is returned as a string.
  Arguments:
  -input_json (str): input filename to process. Defaults to `data.json`;
  -output_json (str): output filename for generated JSON. Defaults to `output.json`;
  -save (boolean): if `True`, save the generated JSON data in `output_json` file and the function returns the filename.
                   Otherwise, the function returns the JSON data in string form. Defaults to `False`.
  Returns:
  - (str) `output_json` filename value if `save` argument is set to `True` or the generated JSON data in string form,
    otherwise.
  """  
  with open(input_json,"r") as input_file:
    data = json.load(input_file)
    articles = data["articles"]
    carts_in = data["carts"]
    delivery_fees = data["delivery_fees"]
    discounts = data["discounts"]
    carts = []  
    for cart in carts_in:
      cart_out = {} 
      total = 0
      cart_out["id"] = cart["id"]
      items = cart["items"]
      for item in items:
        unit_price, quantity = next((article["price"],item["quantity"])  for article in articles if article["id"] == item["article_id"])
        net_price = unit_price
        for discount in discounts:
          if item["article_id"] == discount["article_id"]:            
            if discount["type"] == "amount":            
              net_price = unit_price - discount["value"]
            elif discount["type"] == "percentage":
              net_price = int(unit_price - unit_price*discount["value"]/100.0)
        total += quantity*net_price
      for fee in delivery_fees:
        price = fee["eligible_transaction_volume"]
        if total >= price["min_price"] and total <= ( price["max_price"] if price["max_price"] else float("inf")):
            total += fee["price"]
            break
      cart_out["total"] = total
      carts.append(cart_out)      
    if save:
      with open(output_json,"w+") as output_file:
        json.dump({"carts": carts}, output_file, indent = 2, sort_keys = True, separators = (',', ': '))
        return output_file.name
    else:
      return json.dumps({"carts": carts}, sort_keys = True)

if __name__ == '__main__':
  print(process_json(save=True))
