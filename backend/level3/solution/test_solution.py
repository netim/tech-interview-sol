#!/usr/bin/env python3

"""
Test script based on unittest builtin module.
Author: Medeiros Neto
Environment: Python 3.5 on Linux Mint 18.3
"""

import unittest
import filecmp
from solution import process_json

class TestJSON(unittest.TestCase):
  """
  This script tests the `process_json` function from the `solution` module:
  -The first test compares the returns of the function with and without the `input_json` argument set.
  -The second test compares the returns of the function with and without the `output_json` argument set.
  -The third test compares the contents of the original `output.json` file and generated `output_test_file.json` file.
  """

  def test_input(self):
    self.assertEqual(process_json(input_json="data.json",save=False),process_json(save=False))

  def test_output(self):
    self.assertEqual(process_json(output_json="output.json",save=False),process_json(save=False))

  def test_file(self):
    self.assertTrue(filecmp.cmp("output.json",process_json(output_json="output_test_file.json",save=True),shallow=False))

if __name__ == '__main__':
  unittest.main()
