# Level 3 solution

## How to run

To run the script, open the terminal and execute `./solution.py` on Linux or `python solution.py` on Windows.

To run the tests, open the terminal and execute `./test_solution.py` on Linux or `python test_solution.py` on Windows.

## Development details

### solution.py

`solution.py` is a function to process input JSON file and generate output JSON file.
The input data contains articles information (with `id`, `name` and `price` fields), carts (with `id` and `items`
fields), delivery fees ( with `eligible_transaction_volume` and `price` fields) and discounts (with `article_id`,
`type` and `amount` fields).  The function iterates the items in each cart and gets the `unit_price` and `quantity`.
If the item is eligible for a discount, with matching `article_id`, the discount is applied and returns the trucanted
`net_price`.Then, the `net_price` is multiplied by item `quantity` and is summed in `total`. The delivery fee `price`
is applied if the `total` is between the `min_price` and `max_price` of an `eligible_transaction_volume` iteration.
Then, each cart is returned with the original `id` and `total` value. If the `save` argument is `True`, the 
generated data is converted to a dictonary and saved as a JSON file formatted to match the `output.json` file style
and returns the `output_file` filename. Otherwise, the JSON data is returned as a string.

Arguments:

- input_json (str): input filename to process. Defaults to `"data.json"`;
- output_json (str): output filename for generated JSON. Defaults to `"output.json"`;
- save (boolean): if `True`, save the generated JSON data in `output_json` file and the function returns the filename. Otherwise, the function returns the JSON data in string form. Defaults to `False`.

Returns:

- (str) `output_json` filename value if `save` argument is set to `True` or the generated JSON data in string form,
  otherwise.

### test.solution.py

`test_solution.py` tests the `process_json` function from the `solution` module:

- The first test compares the string returns of the function with and without the `input_json` argument set;
- The second test compares the string returns of the function with and without the `output_json` argument set;
- The third test compares the contents of the original `output.json` file and generated `output_test_file.json` file.

### output.json.bak

`output.json.bak` is the backup of the original `output.json` file.
