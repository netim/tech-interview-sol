#!/usr/bin/env python3

"""
Script for JSON data processing
Author: Medeiros Neto
Environment: Python 3.5 on Linux Mint 18.3
"""

import json
"""
Module for JSON loading, saving and printing.
"""

def process_json(input_json = "data.json", output_json = "output.json", save = False):
  """
  Function to process input JSON file and generate output JSON file.
  The input data contains articles information (with `id`, `name` and `price` fields) and carts information
  (with `id` and `items` fields). The function iterates the `carts` and calculate each `item` price by
  multiplying the item `quantity` field with the article `price` field based on item `article_id` and is summed in `total`.
  Then, each cart is returned with the original `id` and `total` value. If the `save` argument is `True`, the 
  generated data is converted to a dictonary and saved as a JSON file formatted to match the `output.json` file style
  and returns the `output_file` filename. Otherwise, the JSON data is returned as a string.
  Arguments:
  -input_json (str): input filename to process. Defaults to `data.json`;
  -output_json (str): output filename for generated JSON. Defaults to `output.json`;
  -save (boolean): if `True`, save the generated JSON data in `output_json` file and the function returns the filename.
                   Otherwise, the function returns the JSON data in string form. Defaults to `False`.
  Returns:
  - (str) `output_json` filename value if `save` argument is set to `True` or the generated JSON data in string form,
    otherwise.
  """
  with open(input_json,"r") as input_file:
    data = json.load(input_file)
    articles = data["articles"]
    carts_in = data["carts"]
    carts = []  
    for cart in carts_in:
      cart_out = {} 
      total = 0
      cart_out["id"] = cart["id"]
      items = cart["items"]
      for item in items:
        total += next(article["price"]*item["quantity"] for article in articles if article["id"] == item["article_id"])
      cart_out["total"] = total
      carts.append(cart_out)      
    if save:
      with open(output_json,"w+") as output_file:
        json.dump({"carts": carts}, output_file, indent = 2, sort_keys = True, separators = (',', ': '))
        return output_file.name
    else:
      return json.dumps({"carts": carts}, indent = 2, sort_keys = True, separators = (',', ': '))

"""
If the script is run directly, the file `output.json` is generated from the `data.json`.
"""
if __name__ == '__main__':
  print(process_json(save=True))
